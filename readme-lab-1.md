# Team and project
## Team code/name
> Група №9

## Team members list 
1. > Санто	Вероніка	Олександрівна ІП-94
2. > Бондаренко	Анастасія	Тарасівна ІП-96
3. > Куренна	Анна	Вадимівна ІС-91

## Unity version
> 2020.3.28f1

</br>

# Lab
## About team task management (screenshot/description)
https://trello.com/invite/b/7iaLnvzr/a1b62b8bf03134ccf9b7fe0cce2792a1/gamedev

## Chosen games analysis
## Gris
Жанр: <br/>
платформер з головоломками <br/><br/>
Особливість: <br/>
<ul>
<li>захоплюючий стиль графіки та медитативна фонова музика </li>
<li>гра не має жодного слова </li>
</ul><br/>
Ігрові механіки:
<ul>
<li>протягом григоловна героїня здобуває певні навички та відкриває нові кольори для оточення: </li>
	<ul>
	<li>перетворення на камінь</li>
	<li>подвійний стрибок</li>
	<li>плавання</li>
	<li>спів</li>
	<li>зміна гравітації на певних ділянках</li>
	</ul>
<li>на рівнях сховані таємні місця, які можуть бути відкриті якщо з героїні є певні навички </li>
<li>героїня може повертатись до локацій, де вже була,та перепроходити їх заново </li>
<li>на рівнях можуть бути супротивники, що розкривають сюжет, але не вбивають головну героїню </li>
<li>на рівнях можуть буть допоміжні дружні персонажі (наприклад метелики, що допомагають стрибати) </li>
</ul>
## Additional
GameDev channels:<br/>
https://t.me/IndieDevGame<br/>
https://t.me/korovany<br/>
https://www.youtube.com/c/ArtemKorotenko )))<br/>
https://www.youtube.com/channel/UCYbK_tjZ2OrIZFBvU6CCMiA<br/>

Assets sources:<br/>
2d/3d:  https://www.kenney.nl/assets <br/>
2d/3d:  https://itch.io/game-assets/free/tag-unity <br/>
sounds: https://freemusicarchive.org/curator/Video/ <br/>
sounds: https://freesound.org/ <br/>